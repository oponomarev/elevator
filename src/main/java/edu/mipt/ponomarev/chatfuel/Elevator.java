package edu.mipt.ponomarev.chatfuel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.InvalidParameterException;
import java.util.NavigableSet;
import java.util.Optional;
import java.util.TreeSet;

import static edu.mipt.ponomarev.chatfuel.Command.Direction.DOWN;
import static edu.mipt.ponomarev.chatfuel.Command.Direction.UP;

public class Elevator {
	private static final Logger LOGGER = LoggerFactory.getLogger(Elevator.class);
	private static final String INTERNAL_ERROR = "Internal Error";
	public static final int MIN_BUILDING_HEIGHT = 5;
	public static final int MAX_BUILDING_HEIGHT = 20;

	private final int buildingNumber;
	private final double timeThreshold;
	private final double pace;
	private final NavigableSet<Integer> storeysToVisit = new TreeSet<>();

	private volatile Command currentCommand;
	private volatile int currentStorey;

	public Elevator(int buildingNumber, double storeyHeight, double velocity, double timeThreshold) {
		validate(buildingNumber, storeyHeight, velocity, timeThreshold);
		this.buildingNumber = buildingNumber;
		this.timeThreshold = timeThreshold;
		this.pace = storeyHeight / velocity;
		currentStorey = 1;
	}

	private void validate(int buildingNumber, double storeyHeight, double velocity, double timeThreshold) {
		if (buildingNumber < MIN_BUILDING_HEIGHT
				|| buildingNumber > MAX_BUILDING_HEIGHT
				|| storeyHeight <= 0
				|| velocity <= 0
				|| timeThreshold <= 0) {
			throw new InvalidParameterException();
		}
	}

	public synchronized void processCommand(Command command) {
		storeysToVisit.add(command.getStorey());
		this.notifyAll();
	}

	private Optional<Command> nextUpCommand(NavigableSet<Integer> storeysToVisit, int currentStorey) {
		Integer nextUp = storeysToVisit.ceiling(currentStorey);
		if (nextUp == null) {
			return Optional.empty();
		} else {
			storeysToVisit.remove(nextUp);
			return Optional.of(new Command(UP, nextUp));
		}
	}

	private Optional<Command> nextDownCommand(NavigableSet<Integer> storeysToVisit, int currentStorey) {
		Integer nextDown = storeysToVisit.floor(currentStorey);
		if (nextDown == null) {
			return Optional.empty();
		} else {
			storeysToVisit.remove(nextDown);
			return Optional.of(new Command(DOWN, nextDown));
		}
	}

	private Command getNextCommand(NavigableSet<Integer> storeysToVisit,
								   int currentStorey,
								   boolean upFirst) {
		if (storeysToVisit.isEmpty()) {
			return null;
		}
		if (upFirst) {
			return nextUpCommand(storeysToVisit, currentStorey).orElse(
					nextDownCommand(storeysToVisit, currentStorey).orElse(null)
			);
		} else {
			return nextDownCommand(storeysToVisit, currentStorey).orElse(
					nextUpCommand(storeysToVisit, currentStorey).orElse(null)
			);
		}
	}

	void go() {
		while (currentCommand == null) {
			if (storeysToVisit.isEmpty()) {
				synchronized (this) {
					try {
						this.wait();
					} catch (InterruptedException e) {
						LOGGER.error(e.getMessage());
					}
				}
			}
			currentCommand = getNextCommand(storeysToVisit, currentStorey, true);
		}
		switch (currentCommand.getDirection()) {
			case UP:
				while (currentStorey < currentCommand.getStorey()) {
					currentStorey += 1;
					wait(pace);
					LOGGER.info("{}th storey...", currentStorey);
					updateCurrentCommand(currentStorey, currentCommand, storeysToVisit);
				}
				currentCommand = getNextCommand(storeysToVisit, currentStorey, true);
				break;
			case DOWN:
				while (currentStorey > currentCommand.getStorey()) {
					currentStorey -= 1;
					wait(pace);
					LOGGER.info("{}th storey...", currentStorey);
					updateCurrentCommand(currentStorey, currentCommand, storeysToVisit);
				}
				currentCommand = getNextCommand(storeysToVisit, currentStorey, false);
				break;
			case GUESS_YOURSELF:
			default:
				throw new RuntimeException(INTERNAL_ERROR);
		}
		LOGGER.info("Doors are opening");
		wait(timeThreshold);
		LOGGER.info("Doors are closing");
	}

	private void updateCurrentCommand(int currentStorey,
									  Command currentCommand,
									  NavigableSet<Integer> storeysToVisit) {
		Integer currentCommandStorey = currentCommand.getStorey();
		switch (currentCommand.getDirection()) {
			case UP:
				Integer ceiling = storeysToVisit.ceiling(currentStorey);
				if (ceiling != null && ceiling < currentCommandStorey) {
					currentCommand.setStorey(ceiling);
					storeysToVisit.add(currentCommandStorey);
					storeysToVisit.remove(ceiling);
				}
				break;
			case DOWN:
				Integer floor = storeysToVisit.floor(currentStorey);
				if (floor != null && floor > currentCommandStorey) {
					currentCommand.setStorey(floor);
					storeysToVisit.add(currentCommandStorey);
					storeysToVisit.remove(floor);
				}
				break;
			case GUESS_YOURSELF:
			default:
				throw new RuntimeException(INTERNAL_ERROR);
		}
	}

	private void wait(double period) {
		try {
			Thread.sleep((long) (period * 1000));
		} catch (InterruptedException e) {
			//do nothing
		}
	}
}
