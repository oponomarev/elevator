# Elevator

## Prerequisites
__You need JDK-9/JRE-9 to compile/run this program!__

## Description
This is an elevator emulator. It waits for user input and logs elevator's activity in real-time.

## Compilation
_Linux_: `./gradlew clean fatJar`
_Windows_: `gradlew.bat clean fatJar`

## Running
java -jar <jar file name> <parameters>
Parameter list:
1) Number of storeys (5 to 20);
2) Height of a storey (in meters);
3) Elevator speed (in meters per second);
4) Duration when doors are opened (in seconds).

Program always waits for user input after running. You should enter the number of storey to pass a command to elevator.
When elevator is moving it goes to the nearest storey, not the first entered one.